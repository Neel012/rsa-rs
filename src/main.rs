#![windows_subsystem = "windows"]

use relm_attributes::widget;
use relm::*;
use gtk::prelude::*;

mod crypt;
use self::crypt::*;

pub const TITLE: &str = "RSA";

#[derive(relm_derive::Msg)]
pub enum Msg {
    Encrypt,
    Decrypt,
    MessagesView,
    KeysView,
    GenerateKeys,
    ShowPrivateKey,
    ShowPublicKey,
    LoadPrivateKey,
    LoadPublicKey,
    Quit,
}

pub struct Model {
    key_private: RsaKeyPrivate,
    key_public: RsaKeyPublic,
}

#[widget]
impl relm::Widget for Win {
    fn model() -> Model {
        let (private, public) = generate_keys(512);
	Model {
    	    key_private: private,
    	    key_public: public,
        }
    }

    fn update(&mut self, event: Msg) {
        use self::Msg::*;
        use self::crypt::DecryptError::*;
        match event {
            Encrypt => self.write(&self.model.key_public.encrypt(&self.get_input())),
            Decrypt =>
            	match self.model.key_private.decrypt(&self.get_input()) {
                    Ok(s) => self.write(&s),
                    Err(ParseIntError(e)) => self.message_dialog_error(&e.to_string()),
                    Err(FromUtf8Error(e)) => self.message_dialog_error(&e.to_string()),
                }
            MessagesView => {
                self.stack.set_visible_child(&self.messages_view);
                // self.toggle_button_messages.set_active(true);
                // self.toggle_button_keys.set_active(false);
            },
            KeysView => {
                self.stack.set_visible_child(&self.keys_view);
                // self.toggle_button_messages.set_active(false);
                // self.toggle_button_keys.set_active(true);
            },
            GenerateKeys => {
                let buf = self.entry_generate_keys_bits.get_buffer();
                match usize::from_str_radix(&buf.get_text(), 10) {
                    Ok(bits) => if bits >= 32 {
                        let (private, public) = generate_keys(bits);
                        self.model.key_private = private;
                        self.model.key_public = public;
                    },
                    Err(e) => self.message_dialog_error(&e.to_string()),
                }
            },
            ShowPrivateKey => {
                self.textview_private_key_n
                    .get_buffer()
                    .unwrap()
                    .set_text(&self.model.key_private.n.to_str_radix(10, true));
                self.textview_private_key_d
                    .get_buffer()
                    .unwrap()
                    .set_text(&self.model.key_private.d.to_str_radix(10, true));
            },
            ShowPublicKey => {
                self.textview_public_key_n
                    .get_buffer()
                    .unwrap()
                    .set_text(&self.model.key_public.n.to_str_radix(10, true));
                self.textview_public_key_e
                    .get_buffer()
                    .unwrap()
                    .set_text(&self.model.key_public.e.to_str_radix(10, true));
            },
            LoadPrivateKey => {
                let buf_n = self.textview_private_key_n.get_buffer().unwrap();
                let text_n = buf_n.get_text(&buf_n.get_start_iter(), &buf_n.get_end_iter(), false).unwrap_or_default();
                let buf_d = self.textview_private_key_d.get_buffer().unwrap();
                let text_d = buf_d.get_text(&buf_d.get_start_iter(), &buf_d.get_end_iter(), false).unwrap_or_default();
                match RsaKeyPrivate::from_str(&text_n, &text_d) {
                    Ok(key) => self.model.key_private = key,
                    Err(e) => self.message_dialog_error(&e.to_string()),
                }
            },
            LoadPublicKey => {
                let buf_n = self.textview_private_key_n.get_buffer().unwrap();
                let text_n = buf_n.get_text(&buf_n.get_start_iter(), &buf_n.get_end_iter(), false).unwrap_or_default();
                let buf_e = self.textview_private_key_d.get_buffer().unwrap();
                let text_e = buf_e.get_text(&buf_e.get_start_iter(), &buf_e.get_end_iter(), false).unwrap_or_default();
                match RsaKeyPublic::from_str(&text_n, &text_e) {
                    Ok(key) => self.model.key_public = key,
                    Err(e) => self.message_dialog_error(&e.to_string()),
                }
            },
            Quit => gtk::main_quit(),
        }
    }

    fn message_dialog_error(&self, e: &str) {
        let dialog = gtk::MessageDialog::new(
            Some(&self.window),
            gtk::DialogFlags::empty(),
            gtk::MessageType::Error,
            gtk::ButtonsType::Ok,
            e,
        );
        dialog.run();
        dialog.destroy();
    }

    fn write(&self, output: &str) {
        self.text_output.get_buffer().unwrap().set_text(output)
    }

    fn get_input(&self) -> String {
        let buf = match self.text_input.get_buffer() {
            Some(buf) => buf,
            None => return String::new(),
        };
        buf.get_text(&buf.get_start_iter(), &buf.get_end_iter(), false).unwrap_or_default()
    }

    fn init_view(&mut self) {
        // FIXME: modify paned's handle position when window is resized
        self.messages_view.set_position(200);
        self.stack_switcher.set_stack(&self.stack);
    }

    view! {
        #[name="window"]
        gtk::Window {
            title: TITLE,
            decorated: false,
            // resizable: true,
            property_default_height: 650,
            property_default_width: 800,
            gtk::Box {
                orientation: gtk::Orientation::Vertical,
                #[name="titlebar"]
                gtk::HeaderBar {
                    title: TITLE,
                    has_subtitle: false,
                    show_close_button: true,
                    gtk::Button {
                        label: "Encrypt",
                        clicked => Msg::Encrypt,
                    },
                    gtk::Button {
                        label: "Decrypt",
                        clicked => Msg::Decrypt,
                    },
                    #[name="stack_switcher"]
                    gtk::StackSwitcher {
                        #[name="toggle_button_messages"]
                        gtk::ToggleButton {
                            label: "Messages",
                            clicked => Msg::MessagesView,
                        },
                        #[name="toggle_button_keys"]
                        gtk::ToggleButton {
                            label: "Manage keys",
                            clicked => Msg::KeysView,
                        },
                        child: {
                            pack_type: gtk::PackType::End,
                        },
                    },
                },
                #[name="stack"]
                gtk::Stack {
                    child: {
                        expand: true,
                        fill: true,
                    },
                    transition_type: gtk::StackTransitionType::SlideLeft,
                    #[name="messages_view"]
                    gtk::Paned {
                        orientation: gtk::Orientation::Vertical,
                        gtk::Frame {
                            shadow_type: gtk::ShadowType::In,
                            #[name="text_input"]
                            gtk::TextView {
                                monospace: true,
                                wrap_mode: gtk::WrapMode::Char,
                            },
                        },
                        gtk::Frame {
                            shadow_type: gtk::ShadowType::In,
                            #[name="text_output"]
                            gtk::TextView {
                                cursor_visible: false,
                                editable: false,
                                monospace: true,
                                wrap_mode: gtk::WrapMode::Char,
                            },
                        },
                    },
                    #[name="keys_view"]
                    gtk::Grid {
                        valign: gtk::Align::Start,
                        halign: gtk::Align::Center,
                        margin_top: 18,
                        column_spacing: 24,
                            gtk::Button {
                                label: "Generate new keys",
                                clicked => Msg::GenerateKeys,
                                cell: {
                                    left_attach: 0,
                                    top_attach: 0,
                                },
                            },
                            #[name="entry_generate_keys_bits"]
                            gtk::Entry {
                                placeholder_text: "Enter number of bits in decimal format",
                                cell: {
                                    left_attach: 1,
                                    top_attach: 0,
                                },
                            },
                            gtk::Label {
                                markup: "<b><big>Private key</big></b>",
                                cell: {
                                    left_attach: 0,
                                    top_attach: 1,
                                },
                            },
                            gtk::Label {
                                text: "Enter n part in decimal format",
                                line_wrap: true,
                                lines: -1,
                                cell: {
                                    left_attach: 0,
                                    top_attach: 2,
                                },
                            },
                            gtk::Label {
                                text: "Enter d part in decimal format",
                                line_wrap: true,
                                lines: -1,
                                cell: {
                                    left_attach: 1,
                                    top_attach: 2,
                                },
                            },
                            #[name="textview_private_key_n"]
                            gtk::TextView{
                                wrap_mode: gtk::WrapMode::Char,
                                cell: {
                                    left_attach: 0,
                                    top_attach: 3,
                                },
                            },
                            #[name="textview_private_key_d"]
                            gtk::TextView {
                                wrap_mode: gtk::WrapMode::Char,
                                cell: {
                                    left_attach: 1,
                                    top_attach: 3,
                                },
                            },
                            gtk::Button {
                                label: "Load private key",
                                clicked => Msg::LoadPrivateKey,
                                cell: {
                                    left_attach: 2,
                                    top_attach: 3,
                                },
                            },
                            gtk::Button {
                                label: "Show private key",
                                clicked => Msg::ShowPrivateKey,
                                cell: {
                                    left_attach: 3,
                                    top_attach: 3,
                                },
                            },
                            gtk::Label {
                                markup: "<b><big>Public key</big></b>",
                                cell: {
                                    left_attach: 0,
                                    top_attach: 4,
                                },
                            },
                            gtk::Label {
                                text: "Enter n part in decimal format",
                                line_wrap: true,
                                lines: -1,
                                cell: {
                                    left_attach: 0,
                                    top_attach: 5,
                                },
                            },
                            gtk::Label {
                                text: "Enter e part in decimal format",
                                line_wrap: true,
                                lines: -1,
                                cell: {
                                    left_attach: 1,
                                    top_attach: 5,
                                },
                            },
                            #[name="textview_public_key_n"]
                            gtk::TextView{
                                wrap_mode: gtk::WrapMode::Char,
                                cell: {
                                    left_attach: 0,
                                    top_attach: 6,
                                },
                            },
                            #[name="textview_public_key_e"]
                            gtk::TextView {
                                wrap_mode: gtk::WrapMode::Char,
                                cell: {
                                    left_attach: 1,
                                    top_attach: 6,
                                },
                            },
                            gtk::Button {
                                label: "Load public key",
                                clicked => Msg::LoadPublicKey,
                                cell: {
                                    left_attach: 2,
                                    top_attach: 6,
                                },
                            },
                            gtk::Button {
                                label: "Show public key",
                                clicked => Msg::ShowPublicKey,
                                cell: {
                                    left_attach: 3,
                                    top_attach: 6,
                                },
                            },
                        },
                },
            },
            delete_event(_, _) => (Msg::Quit, gtk::Inhibit(false)),
        }
    }
}

fn main() {
    Win::run(()).expect("Failed to setup GUI");
}
