use ramp::{Int, RandomInt, int::ParseIntError};

fn gen_prime(bitsize: usize) -> Int {
    let mut rng = rand::thread_rng();
    loop {
        let mut p = rng.gen_uint(bitsize);
        p.set_bit(0, true);
        p.set_bit((bitsize - 1) as u32, true);
        debug_assert_eq!(p.bit_length(), bitsize as u32);
        if is_prime(&p) {
            return p;
        }
    }
}

fn is_prime_naive(p: &Int) -> bool {
    if p <= &3 {
        return p > &1;
    }
    if p.is_even() || p % 3 == 0 {
        return false;
    }
    let mut i = Int::from(5);
    while &i.clone().square() <= p {
        if p % i.clone() == 0 || p % (i.clone() + 2) == 0 {
            return false;
        }
        i += 6;
    }
    return true
}

fn is_prime(p: &ramp::Int) -> bool {
    miller_rabin(&p)
}

// source: https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
//
// write n − 1 as 2r·d with d odd by factoring powers of 2 from n − 1
// WitnessLoop: repeat k times:
//    pick a random integer a in the range [2, n − 2]
//    x ← a^d mod n
//    if x = 1 or x = n − 1 then
//       continue WitnessLoop
//    repeat r − 1 times:
//       x ← x^2 mod n
//       if x = n − 1 then
//          continue WitnessLoop
//    return composite
// return probably prime
//

/// check wether odd number `n` is a prime
fn miller_rabin(n: &Int) -> bool {
    const K: usize = 256;
    let mut rng = rand::thread_rng();
    let n_subone = n - Int::one();
    let (r, d): (Int, Int) = {
        let mut r = Int::from(1);
        let mut d = n_subone.clone();
        while d.is_even() {
            d = d / 2;
            r += 1;
        }
        (r / 2, d)
    };
    for _ in 0..K {
        let a: Int = rng.gen_int_range(&Int::from(2), &n_subone);
        let mut x = a.pow_mod(&d, &n);
        if x == 1 || x == n_subone {
            continue;
        }
        for _ in Int::zero()..(r - Int::one()) {
            x = x.dsquare() & 2;
            if x == n_subone {
                continue;
            }
        }
        return false;
    }
    true
}

// function inverse(a, n)
// t := 0;     newt := 1;    
// r := n;     newr := a;    
// while newr ≠ 0
//     quotient := r div newr
//     (t, newt) := (newt, t - quotient * newt) 
//     (r, newr) := (newr, r - quotient * newr)
// if r > 1 then return "a is not invertible"
// if t < 0 then t := t + n
// return t
fn mmi(a: Int, n: Int) -> Int {
    let mut t = (Int::zero(), Int::one());
    let mut r = (n.clone(), a);
    while r.1 != Int::zero() {
        let q = r.0.clone() / r.1.clone();
        t = (t.1.clone(), t.0 - q.clone() * t.1);
        r = (r.1.clone(), r.0 - q * r.1);
    }
    if &r.0 > &Int::one() {
        panic!("a is not invertible");
    }
    if &t.0 < &Int::zero() {
        t.0 += n;
    }
    t.0
}

// struct RsaKeyPair {
//     n: Int,
//     e: Int,
//     d: Int,
// }

pub struct RsaKeyPrivate {
    pub n: Int,
    pub d: Int,
}

pub struct RsaKeyPublic {
    pub n: Int,
    pub e: Int,
}

pub fn generate_keys(key_bitsize: usize) -> (RsaKeyPrivate, RsaKeyPublic) {
    let bitsize = key_bitsize / 2;
    let mut rng = rand::thread_rng();
    let p = gen_prime(bitsize);
    let q = gen_prime(bitsize);
    let n = p.clone() * q.clone();
    let phi = (p - Int::one()) * (q - Int::one());
    let e = loop {
        let e = rng.gen_int_range(&Int::from(1024), &phi);
        if phi.gcd(&e) == Int::one() { break e; }
    };
    let private = RsaKeyPrivate {
        n: n.clone(),
        d: mmi(e.clone(), phi),
    };
    let public = RsaKeyPublic {
        n: n,
        e: e
    };
    (private, public)
}

impl RsaKeyPublic {
    pub fn encrypt(&self, text: &str) -> String {
        let mut out = String::new();
        let bitlen = self.n.bit_length() as usize;
        let chunk_bytelen = (bitlen - 1) / 8;
        for chunk in text.as_bytes().chunks(chunk_bytelen) {
            let mut msg = Int::zero();
            for (i, byte) in chunk.iter().enumerate() {
                msg += Int::from(*byte) << (i * 8);
            }
            debug_assert!(&msg < &self.n);
            let encrypted = msg.pow_mod(&self.e, &self.n);
            out.push_str(&format!("{:01$b}", encrypted, bitlen));
        }
        out
    }

    pub fn from_str(n: &str, e: &str) -> Result<RsaKeyPublic, ParseIntError> {
        let n = Int::from_str_radix(n, 10)?;
        let e = Int::from_str_radix(e, 10)?;
        Ok(RsaKeyPublic {
            n: n,
            e: e,
        })
    }
}

impl RsaKeyPrivate {
    pub fn decrypt(&self, text: &str) -> Result<String, DecryptError> {
        let mut out: Vec<u8> = Vec::new();
        let bitlen = self.n.bit_length() as usize;
        for chunk in text.as_bytes().chunks(bitlen) {
            let chunk = std::str::from_utf8(chunk).unwrap();
            let encrypted = Int::from_str_radix(chunk, 2)?;
            let mut msg = encrypted.pow_mod(&self.d, &self.n);
            while msg != Int::zero() {
                out.push(u8::from(&msg));
                msg >>= 8;
            }
        }
        Ok(String::from_utf8(out)?)
    }

    pub fn from_str(n: &str, d: &str) -> Result<RsaKeyPrivate, ParseIntError> {
        let n = Int::from_str_radix(n, 10)?;
        let d = Int::from_str_radix(d, 10)?;
        Ok(RsaKeyPrivate {
            n: n,
            d: d,
        })
    }
}

pub enum DecryptError {
    ParseIntError(ramp::int::ParseIntError),
    FromUtf8Error(std::string::FromUtf8Error),
}

impl From<ParseIntError> for DecryptError {
    fn from(err: ParseIntError) -> Self {
        DecryptError::ParseIntError(err)
    }
}

impl From<std::string::FromUtf8Error> for DecryptError {
    fn from(err: std::string::FromUtf8Error) -> Self {
        DecryptError::FromUtf8Error(err)
    }
}
