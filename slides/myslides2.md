---
author: Ondra Matušák
title: RSA
subtitle: Kryptologie
---
# Tools
* Language: Rust
* Toolkit: GTK
* Libraries: Relm, Ramp

---
# Generating Primes
```rust
fn gen_prime(bitsize: usize) -> Int {
    loop {
        let mut p = rng.gen_uint(bitsize);
        p.set_bit(0, true);
        p.set_bit((bitsize - 1) as u32, true);
        debug_assert_eq!(p.bit_length(), bitsize as u32);
        if is_prime(&p) { return p; }
    }
}
```

---
# Naive Primes
```rust
fn is_prime_naive(p: &Int) -> bool {
    if p <= &3 {
        return p > &1;
    }
    if p.is_even() || p % 3 == 0 {
        return false;
    }
    let mut i = Int::from(5);
    while &i.clone().square() <= p {
        if p % i.clone() == 0 || p % (i.clone() + 2) == 0 {
            return false;
        }
        i += 6;
    }
    return true
}
```

---
# Miller-Rabin
- probabilistic primality test
- source: https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
```rust
fn is_prime(p: &ramp::Int) -> bool {
    miller_rabin(&p)
}
```

---
# Miller-Rabin II.
```rust
fn miller_rabin(n: &Int) -> bool {
    const K: usize = 256;
    let mut rng = rand::thread_rng();
    let n_subone = n - Int::one();
    let (r, d): (Int, Int) = {
        let mut r = Int::from(1);
        let mut d = n_subone.clone();
        while d.is_even() {
            d = d / 2;
            r += 1;
        }
        (r / 2, d)
    };
    for _ in 0..K {
        let a: Int = rng.gen_int_range(&Int::from(2), &n_subone);
        let mut x = a.pow_mod(&d, &n);
        if x == 1 || x == n_subone {
            continue;
        }
        for _ in Int::zero()..(r - Int::one()) {
            x = x.dsquare() & 2;
            if x == n_subone {
                continue;
            }
        }
        return false;
    }
    true
}
```

---
# MMI
* Modular multiplicative inversion
asd

---
# II. MMI
```rust
fn mmi(a: Int, n: Int) -> Int {
    let mut t = (Int::zero(), Int::one());
    let mut r = (n.clone(), a);
    while r.1 != Int::zero() {
        let q = r.0.clone() / r.1.clone();
        t = (t.1.clone(), t.0 - q.clone() * t.1);
        r = (r.1.clone(), r.0 - q * r.1);
    }
    if &r.0 > &Int::one() {
        panic!("a is not invertible");
    }
    if &t.0 < &Int::zero() {
        t.0 += n;
    }
    t.0
}
```

---
# Key pair representation
```rust
pub struct RsaKeyPrivate {
    pub n: Int,
    pub d: Int,
}

pub struct RsaKeyPublic {
    pub n: Int,
    pub e: Int,
}
```

---
# Interface
```rust
impl RsaKeyPublic {
    pub fn encrypt(&self, text: &str) -> String  {...}
    pub fn from_str(n: &str, e: &str) -> Result<RsaKeyPublic, ParseIntError> {...}
}

impl RsaKeyPrivate {
    pub fn decrypt(&self, text: &str) -> Result<String, DecryptError> {...}
    pub fn from_str(n: &str, d: &str) -> Result<RsaKeyPrivate, ParseIntError> {...}
}
```

---
# GUI
* paradigm: model view update

---
# Model
```rust
pub struct Model {
    key_private: RsaKeyPrivate,
    key_public: RsaKeyPublic,
}
```

---
# Relm model
```rust
fn model() -> Model {
    let (private, public) = generate_keys(512);
Model {
	    key_private: private,
	    key_public: public,
    }
}
```

---
# Update
```rust
#[derive(relm_derive::Msg)]
pub enum Msg {
    Encrypt,
    Decrypt,
    ...
    GenerateKeys,
    LoadPrivateKey,
    LoadPublicKey,
    Quit,
}
```

---
# Relm update
```rust
fn update(&mut self, event: Msg) {
    match event {
        Encrypt => self.write(&self.model.key_public.encrypt(&self.get_input())),
        Decrypt =>
        	match self.model.key_private.decrypt(&self.get_input()) {
                Ok(s) => self.write(&s),
                Err(ParseIntError(e)) => self.message_dialog_error(&e.to_string()),
                Err(FromUtf8Error(e)) => self.message_dialog_error(&e.to_string()),
            }
        ...
        GenerateKeys => {
            let buf = self.entry_generate_keys_bits.get_buffer();
            match usize::from_str_radix(&buf.get_text(), 10) {
                Ok(bits) => if bits >= 32 {
                    let (private, public) = generate_keys(bits);
                    self.model.key_private = private;
                    self.model.key_public = public;
                },
                Err(e) => self.message_dialog_error(&e.to_string()),
            }
        },
        LoadPrivateKey => {
            ...
            match RsaKeyPrivate::from_str(&text_n, &text_d) {
                Ok(key) => self.model.key_private = key,
                Err(e) => self.message_dialog_error(&e.to_string()),
            }
        },
    }
}
```

---
# Relm update 2
```rust
fn update(&mut self, event: Msg) {
    match event {
        ...
        GenerateKeys => {
            let buf = self.entry_generate_keys_bits.get_buffer();
            match usize::from_str_radix(&buf.get_text(), 10) {
                Ok(bits) => if bits >= 32 {
                    let (private, public) = generate_keys(bits);
                    self.model.key_private = private;
                    self.model.key_public = public;
                },
                Err(e) => self.message_dialog_error(&e.to_string()),
            }
        },
        LoadPrivateKey => {
            ...
            match RsaKeyPrivate::from_str(&text_n, &text_d) {
                Ok(key) => self.model.key_private = key,
                Err(e) => self.message_dialog_error(&e.to_string()),
            }
        },
    }
}
```

---
# Relm view
```rust
view! {
    gtk::Window {
        gtk::Box {
            orientation: gtk::Orientation::Vertical,
            gtk::HeaderBar {
                title: TITLE,
                has_subtitle: false,
                show_close_button: true,
                gtk::Button {
                    label: "Encrypt",
                    clicked => Msg::Encrypt,
                },
                gtk::Button {
                    label: "Decrypt",
                    clicked => Msg::Decrypt,
                },
            },},},
}
```

---
# Relm view 2
```rust
#[name="stack"]
gtk::Stack {
    transition_type: gtk::StackTransitionType::SlideLeft,
    #[name="messages_view"]
    gtk::Paned {
        orientation: gtk::Orientation::Vertical,
        gtk::Frame {
            shadow_type: gtk::ShadowType::In,
            #[name="text_output"]
            gtk::TextView {
                cursor_visible: false,
                editable: false,
                monospace: true,
                wrap_mode: gtk::WrapMode::Char,
            },
        },
    },
},
```
---
# Run
```rust
fn main() {
    Win::run(()).expect("Failed to setup GUI");
}
```
